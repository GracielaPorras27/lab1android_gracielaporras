package com.example.lab1retrofit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.lab1retrofit.Model.Country;
import com.example.lab1retrofit.io.CountriesApiAdapter;
import com.example.lab1retrofit.io.CountryListAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<ArrayList<Country>> {

    private RecyclerView Country_List;
    CountryListAdapter adapter;
    private List<Country> countriesList;
    ConstraintLayout itemLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        countriesList = new ArrayList<>();

        Country_List = (RecyclerView) findViewById(R.id.Country_info);

        Country_List.setLayoutManager(new LinearLayoutManager(this));

        adapter= new CountryListAdapter(this, countriesList);
        Country_List.setAdapter(adapter);

        Call<ArrayList<Country>> call = CountriesApiAdapter.getApiService().getCountries();
        call.enqueue(this);
        Log.d("salida", call.toString());

    }

    @Override
    public void onResponse(Call <ArrayList<Country>> call, Response<ArrayList<Country>> response){
        if(response.isSuccessful()){
            List<Country> postList = response.body();
            for(Country country: postList){
                countriesList.add(country);
            }
            adapter.notifyDataSetChanged();
        }else{
            System.out.println(response.errorBody());
        }
}

    @Override
    public void onFailure(Call<ArrayList<Country>> call, Throwable t) {

    }

}
