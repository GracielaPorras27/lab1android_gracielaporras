package com.example.lab1retrofit.io;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab1retrofit.CountriesInfoActivity;
import com.example.lab1retrofit.Model.Country;
import com.example.lab1retrofit.R;

import java.util.List;

public class CountryListAdapter extends RecyclerView.Adapter<CountryListAdapter.CountryViewHolder> {

    private List<Country> countriesList;
    private Context context;
    CountryViewHolder viewHolder;
    public CountryListAdapter(Context context, List<Country> countriesList) {
        this.countriesList = countriesList;
        this.context=context;
    }


    /*
    Permite construir cada item en base al layout que asigne
    */
    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View listItem = inflater.inflate(R.layout.country_list_item, parent, false);
        CountryViewHolder viewHolder = new CountryViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CountryViewHolder holder, int position) {

        final Country country = countriesList.get(position);
        holder.name.setText(country.getCountry());


        holder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(context, CountriesInfoActivity.class);
                intent.putExtra("name", country.getCountry());
                v.getContext().startActivity(intent);
            }
        });
    }
    /*
     Me permite conocer el tamaño de la lista en tiempo real
     */
    @Override
    public int getItemCount() {
        return countriesList.size();
    }


    //View holder para lograr llenar el contenido de cada item
    public static class CountryViewHolder extends RecyclerView.ViewHolder{

        TextView name;
        ConstraintLayout itemLayout;


        public CountryViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.tv_nameC);
            this.itemLayout = (ConstraintLayout) itemView.findViewById(R.id.cl_country_list_item);
        }

    }

}