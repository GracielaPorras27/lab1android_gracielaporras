package com.example.lab1retrofit.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CountriesApiAdapter {

    private static CountriesApiService API_SERVICE;

    public static CountriesApiService getApiService() {

        // Creamos un interceptor y le indicamos el log level a usar
        //HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        // Asociamos el interceptor a las peticiones
        //OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        //httpClient.addInterceptor(logging);

        Gson gson = new GsonBuilder().setLenient().create();
        String baseUrl = "https://api.covid19api.com/";

        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    //.client(httpClient.build()) // <-- usamos el log level
                    .build();
            API_SERVICE = retrofit.create(CountriesApiService.class);
        }

        return API_SERVICE;
    }

}


