package com.example.lab1retrofit.io;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab1retrofit.Model.Information;
import com.example.lab1retrofit.R;

import java.util.List;

public class CountriesInfoAdapter extends RecyclerView.Adapter<CountriesInfoAdapter.CountryViewHolder>{

    private List<Information> countriesInfo;
    private Context context;

    public CountriesInfoAdapter(Context context, List<Information> countriesInfo) {
        this.countriesInfo = countriesInfo;
        this.context=context;
    }


    /*
    Permite construir cada item en base al layout que asigne
    */
    @NonNull
    @Override
    public CountriesInfoAdapter.CountryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View listItem = inflater.inflate(R.layout.select_country_info, parent, false);
        CountriesInfoAdapter.CountryViewHolder viewHolder = new CountriesInfoAdapter.CountryViewHolder(listItem);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CountriesInfoAdapter.CountryViewHolder holder, int position) {

        final Information information = countriesInfo.get(position);
        holder.name.setText(information.getCountry());
        holder.codeC.setText(information.getCountryCode());
        holder.province.setText(information.getProvince());
        holder.city.setText(information.getCity());
        holder.cityCode.setText(information.getCityCode());
        holder.lat.setText(information.getLat());
        holder.lon.setText(information.getLon());
        holder.cases.setText(information.getCases());
        holder.status.setText(information.getStatus());
        holder.date.setText(information.getDate());
    }

    /*
     Me permite conocer el tamaño de la lista en tiempo real
     */
    @Override
    public int getItemCount() {
        return countriesInfo.size();
    }


    //View holder para lograr llenar el contenido de cada item
    public static class CountryViewHolder extends RecyclerView.ViewHolder{

        public TextView name;
        public TextView codeC;
        public TextView province;
        public TextView city;
        public TextView cityCode;
        public TextView lat;
        public TextView lon;
        public TextView cases;
        public TextView status;
        public TextView date;

        public ConstraintLayout itemLayout;


        public CountryViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name = (TextView) itemView.findViewById(R.id.tv_nameC);
            this.codeC = (TextView) itemView.findViewById(R.id.tv_CountryCode);
            this.province = (TextView) itemView.findViewById(R.id.tv_Province);
            this.city = (TextView) itemView.findViewById(R.id.tv_City);
            this.cityCode = (TextView) itemView.findViewById(R.id.tv_CityCode);
            this.lat = (TextView) itemView.findViewById(R.id.tv_Lat);
            this.lon = (TextView) itemView.findViewById(R.id.tv_Lon);
            this.cases = (TextView) itemView.findViewById(R.id.tv_Cases);
            this.status = (TextView) itemView.findViewById(R.id.tv_Status);
            this.date = (TextView) itemView.findViewById(R.id.tv_Date);

            this.itemLayout = (ConstraintLayout) itemView.findViewById(R.id.cl_country_list_info);
        }
    }
}
