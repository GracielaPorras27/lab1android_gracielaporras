package com.example.lab1retrofit.io;

import com.example.lab1retrofit.Model.Country;
import com.example.lab1retrofit.Model.Information;

import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CountriesApiService {

    @GET("countries")
    Call<ArrayList<Country>> getCountries();

    @GET("country/{name}/status/confirmed")
    Call<ArrayList<Information>> getByCountry(@Path("name") String first, @Query("from") String second, @Query("to") String third);


}