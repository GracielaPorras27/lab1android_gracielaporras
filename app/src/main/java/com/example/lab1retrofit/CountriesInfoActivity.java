package com.example.lab1retrofit;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab1retrofit.Model.Information;
import com.example.lab1retrofit.io.CountriesApiAdapter;
import com.example.lab1retrofit.io.CountriesInfoAdapter;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CountriesInfoActivity extends AppCompatActivity implements Callback<ArrayList<Information>> {
    private RecyclerView Country_info;
    CountriesInfoAdapter countriesInfoAdapter;
    ArrayList<Information> listInformation;
    static final String POST_ID="name";
    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_info);

        listInformation = new ArrayList<>();

        Country_info = (RecyclerView) findViewById(R.id.Country_info);

        Country_info.setLayoutManager(new LinearLayoutManager(this));

        countriesInfoAdapter= new CountriesInfoAdapter(this, listInformation);
        Country_info.setAdapter(countriesInfoAdapter);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = new Date();
        Date ayer = new Date( date.getTime()-86400000);

        String fecha = dateFormat.format(date)+"T00:00:00Z";
        String fecha_anterior = dateFormat.format(ayer)+"T00:00:00Z";

        Log.d("salidaaaaaaaaaaaaaaaaaaa", fecha_anterior);

        String getName;
        Intent intent= getIntent();

        if (intent != null){
            getName=intent.getStringExtra("name");
            Call<ArrayList<Information>> call = CountriesApiAdapter.getApiService().getByCountry(getName, fecha_anterior, fecha);
            call.enqueue(this);

        }

    }

    @Override
    public void onResponse(Call <ArrayList<Information>> call, Response<ArrayList<Information>> response){
        if(response.isSuccessful()){
            List<Information> postList = response.body();
            for(Information information: postList){
                listInformation.add(information);
            }
            countriesInfoAdapter.notifyDataSetChanged();
        }else{
            System.out.println(response.errorBody());
        }
    }

    @Override
    public void onFailure(Call<ArrayList<Information>> call, Throwable t) {

    }
}
